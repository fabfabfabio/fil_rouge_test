<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="css/css du compte.css">
<link rel="stylesheet" type="text/css" href="css/w3-theme-blue-grey.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/update.css">
<title>My account</title>
</head>

<body class="w3-theme-d1">
<div class="menubody">
	<!-- MENU -->
<jsp:include page="menu.jsp" />

	<form action="update.do" method="post">
		<div class="container">
			<div class="row2">
				
					<fieldset>

						<%@ page import="java.util.*"
						import="com.afpa.idp.dao.*"
						import="com.afpa.idp.classes.*"
						import= "java.sql.*"
						import="org.json.JSONException"%>
						<%
						Connexion co = new Connexion();
						Connection connexion = co.getConnection();
						
						session = request.getSession();
						
						DAO<Client> clientDao = new ClientDAO(connexion);
						User user =(User) session.getAttribute("user");
						System.out.println(user.getUserId());
						Client cli = clientDao.find(((User) (session.getAttribute("user"))).getUserId());
						System.out.println(cli.getName());
						%>

						<!-- Client Account form -->

						<h2>Account informations</h2>

						<!-- name (nom de famille) input-->
						<div class="control-group">
							<label class="control-label">Name</label>
							<div class="controls">
								<input value="<%out.print(cli.getName());%>" id="name" name="Name" type="text" placeholder="Name"
									class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>

						<!-- First name (prénom) input-->
						<div class="control-group">
							<label class="control-label">First name</label>
							<div class="controls">
								<input value="<%out.print(cli.getFirstname());%>" id="First name" name="Firstname" type="text"
									placeholder="First name" class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>

						<!-- mail input-->
						<div class="control-group">
							<label class="control-label">Mail :</label>
							<div class="controls">
								<input value="<%out.print(cli.getMail());%>" id="Mail" name="user_mail" type="text" placeholder="Mail"
									class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>

						<!-- phone input-->
						<div>
							<label>Phone :</label>
							<div>
								<input value="<%out.print(cli.getPhone());%>" id="phone" name="phone" type="text" placeholder="Mail"
									class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>

						<!-- address input-->
						<div class="control-group">
							<label class="control-label">Address : Apartment, suite,
								unit, building, floor, etc...</label>
							<div class="controls">
								<input value="<%out.print(cli.getAddress());%>" id="address" name="address"
									type="text" placeholder="address" class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>

						<!-- Birthday input-->
						<div class="control-group">
							<label class="control-label">Birthday</label>
							<div class="controls">
								<input value="<%out.print(cli.getBirthdate());%>" id="Birthday" name="Birthday" type="date"
									class="input-xlarge" pattern="[0-9]{2}-[0-9]{2}-[0-9]{4}" />
								<p class="help-block"></p>
							</div>
						</div>


						<!-- city input-->
						<div class="control-group">
							<label class="control-label">City / Town</label>
							<div class="controls">
								<input value="<%out.print(cli.getVille());%>" id="city" name="city" type="text" placeholder="city"
									class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>

						<!-- postal-code input-->
						<div class="control-group">
							<label class="control-label">Zip / Postal Code</label>
							<div class="controls">
								<input value="<%out.print(cli.getZipcode());%>" id="postal-code" name="postal-code" type="text"
									placeholder="zip or postal code" class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>

						<!-- postal-code input-->
						<div class="control-group">
							<label class="control-label">Country</label>
							<div class="controls">
								<input value="<%out.print(cli.getPays());%>" id="country" name="country" type="text"
									placeholder="country" class="input-xlarge">
								<p class="help-block"></p>
							</div>
						</div>
							<br /> <br /> <input class="refresh" type="submit" name="Submit" />
						
					</fieldset>
				
			</div>
		</div>
	</form>
	</div>
</body>
</html>