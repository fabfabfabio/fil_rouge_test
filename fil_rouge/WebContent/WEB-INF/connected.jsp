<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V02</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="css/w3-theme-blue-grey.css">

</head>



<body id="56" class="w3-theme-d1">
	<script type="text/javascript">
	function selectedService(){
		var radios = document.getElementsByName('service');
		var valeur;
		for(var i = 0; i < radios.length; i++){
		 if(radios[i].checked){
		 valeur = radios[i].value;
		 }
		}
		return valeur;
	}
	
		//Fonctions AJAX
			function getXMLHttpRequest() {
				var xhr = null;
				if (window.XMLHttpRequest || window.ActiveXObject) {
					if (window.ActiveXObject) {
						try {
							xhr = new ActiveXObject("Msxml2.XMLHTTP");
						} catch(e) {
							xhr = new ActiveXObject("Microsoft.XMLHTTP");
						}
					} else {
						xhr = new XMLHttpRequest(); 
					}
				} else {
					alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
					return null;
				}
				return xhr;
			}


			function choixLaunchService(id) {
				var xhr = getXMLHttpRequest();
				var url = "connected.do?servlaunch=" + escape(id);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
						document.getElementById('main_content').innerHTML = xhr.responseText; // Données textuelles récupérées
						//test
					}
				};
				xhr.open("GET", url, true);
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.send("servlaunch=" + id);
			}
			
			function choixStopService(id) {
				var xhr = getXMLHttpRequest();
				var url = "connected.do?servstop=" + escape(id);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
						document.getElementById('main_content').innerHTML = xhr.responseText; // Données textuelles récupérées
						//test
					}
				};
				xhr.open("GET", url, true);
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.send("servstop=" + id);
			}
			
			function refreshService(id) {
				var xhr = getXMLHttpRequest();
				var url = "connected.do?servrefresh=" + escape(id);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
						document.getElementById('56').innerHTML = xhr.responseText; // Données textuelles récupérées
						//test
					}
				};
				xhr.open("GET", url, true);
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.send("servrefresh=" + id);
			}
			
			function subscribeService(id, idUser) {
				var xhr = getXMLHttpRequest();
				var url = "connected.do?subscribeservice=" + escape(id) + "&iduser="+ escape(idUser);
				xhr.onreadystatechange = function() {
					if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
						document.getElementById('56').innerHTML = xhr.responseText; // Données textuelles récupérées
						//test
					}
				};
				xhr.open("GET", url, true);
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				xhr.send("subscribeservice=" + id+ "&iduser=" + idUser);
				
			}
		</script>
	<!-- MENU -->
<div class="menubody">	
<jsp:include page="menu.jsp" />

	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
					<div id="services" class="table">
					<%@ page import="java.util.*"
					import ="com.afpa.idp.dao.*"
					import ="com.afpa.idp.classes.*"
					import ="com.afpa.idp.proxmox.*"
					import ="org.json.JSONException"
					%>
					<div class="row header">
						<div class="cell">
						Service Name
						</div>
						<div class="cell">
						Start
						</div>
						<div class="cell">
						Stop
						</div>
						<div class="cell">
						Status
						</div>
					</div>
					
					<jsp:useBean id="user" beanName="user" scope="session" type="com.afpa.idp.classes.User"/>
					<c:forEach items="${user.subscriptions}" var="subscription" >
						<c:set var="running" scope="page" value="Stopped"/>
						<c:if test = "${subscription.service.isOn()}">
							<c:set var="running" scope="page" value="Started"/>
						</c:if>
				         <div class="row">
				         
				         	
				         	<div class="cell">
				         		<c:out value="${subscription.service.serviceName}"/> 
				         	</div>
				         	<div class="cell">
				         		<input class="start" id="<c:out value="${subscription.service.serviceId}"/>"  onClick=" choixLaunchService(this.id); " type="button" value="Start service"> 
				         	</div>
				         	<div class="cell">
				         		<input class="stop" id="<c:out value="${subscription.service.serviceId}"/>"  onClick=" choixStopService(this.id); " type="button" value="Stop service"> 
				         	</div>
				         	<div class="cell">
				         		<c:out value="${running}"/>
				         	</div>
				     	 </div>
				     	 
				     	  
					</c:forEach>
							<div class="cell">
								<br><br> 
								<input class="refresh" id="<c:out value="${user.userId}"/>" onClick=" refreshService(${user.userId});" type="button" value="Actualiser">
						  </div>		
						

				
	</div>
</div>
<div class="wrap-table100">
	<br><br>
					<div id="services" class="table">
					<form id ="servX">
					<div class="row header">
						<div class="cell">
						Subscribe To
						</div>
						<div class="cell">
						
						</div>
						<div class="cell">
							Price
						</div>
					</div>
					
					
					
					<%
						ArrayList<Service> allServices= (ArrayList<Service>) request.getAttribute("services");
						for(Subscription subs: user.getSubscriptions()){
							if(allServices.contains(subs.getService())){
								allServices.remove(subs.getService());
							}
						} 
						request.setAttribute("services", allServices);
						%>
						
						<c:forEach items="${services}" var="service" >
						
							<div class="row">  
							
								<div class="cell">
									<input type="radio" id="<c:out value="${service.serviceId}"/>" name="service" value="<c:out value="${service.serviceId}"/>">
								</div>
								<div class="cell">
									<label for="<c:out value="${service.serviceId}"/>"><c:out value="${service.serviceId}"/> <c:out value="${service.serviceName}"/></label>
								</div>	
								<div class="cell">
									<label for="<c:out value="${service.serviceId}"/>"><c:out value="${service.price}"/></label>
								</div>
							</div>
						</c:forEach>
						<br><br>
						<input class="subscribe" id="12"  onClick="var r = confirm('Etes vous sur de vouloir souscrire à ce service ?');if (r == true) {subscribeService(selectedService(),${user.userId})};"  type="button" value="Souscrire">
						
				
					</form>
					</div>
			</div>		
		</div>		
	</div>
</div>	
	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	

</body>
</html>