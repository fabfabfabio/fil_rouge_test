package com.afpa.idp.ext;

import java.util.Date;
import java.util.regex.*;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Session;
import javax.mail.MessagingException;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.Address;
import javax.mail.internet.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;

/**
 * <b>Tools is the class regrouping all the static methods which will be used in
 * other classes of the project.</b>
 * 
 * 
 * @author Group 1
 *
 */

public class Tools {

	/**
	 * pathFileLogs is the path of the file containing all the logs information.
	 * 
	 * @see Tools#writeLogs(int, String, String)
	 */
	private static String pathFileLogs = "C:\\Users\\Dorian\\git\\fil_rouge_dorian\\fil_rouge\\conf\\traces.log";

	/**
	 * Static method to create (if the logs file doesn't exist) and to complete it
	 * with new logs information.
	 * 
	 * @author N'AASSI A�cha
	 * @since 18/04/2019
	 *
	 * @param userId   : the Identification's number of the user
	 * @param action   : to describe the action(s) made ; for example :create a new
	 *                 user.
	 * @param comments : to mention others informations
	 * 
	 * 
	 * 
	 */
	public static void writeLogs(int userId, String action, String comments) throws IOException {

		SimpleDateFormat formater = null;
		Date aujourdhui = new Date();
		formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");

		FileWriter fw = new FileWriter(pathFileLogs, true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(formater.format(aujourdhui) + ";" + userId + ";" + action + ";" + comments);
		bw.newLine();
		bw.close();

	}
	
	public static void writeLogs(int userId, String action, String comments, String path) throws IOException {

		SimpleDateFormat formater = null;
		Date aujourdhui = new Date();
		formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");

		FileWriter fw = new FileWriter(path, true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(formater.format(aujourdhui) + ";" + userId + ";" + action + ";" + comments);
		bw.newLine();
		bw.close();

	}
	
	/**
	 * Method static for control to e-mail, respect the standard spelling
	 * 
	 * @author Hus Sebastien
	 * 
	 * @since 2019/04/17
	 * 
	 * @param mail    : the e-mail testing
	 * @param maxSize : determined the length of e-mail which is allowed in the data
	 *                base
	 * 
	 * @return : boolean equal true if the e-mail respect standard spelling, else
	 *         boolean equal false
	 */
	public static boolean controlMail(String mail, int maxSize) {
		boolean result;

		if (mail.length() <= maxSize) {
			String regex = "^[a-z0-9.-]+@[a-z0-9.-]{2,}(\\.[a-z]{2,4})$";
			result = Pattern.matches(regex, mail);
		} else {
			result = false;
		}

		return result;
	}


	/**
	* Method static to read config.properties
	* @author: Sofiane, Ndenga, Alex, Fabrice
	* @param keyParam
	* @return value corresponding to the parameter
	 * @throws FileNotFoundException 
	*/	
	 public static String readFile(String keyParam, String path) throws FileNotFoundException {
	        String value="";
	        Properties properties = new Properties();

	        String dest= "";

	        String osName = System.getProperty("os.name");
	        String file = "config.properties";

	        if (osName.equals("Mac OS X")) {
	            dest = path + "\\" + file;
	        } else {
	            dest = path + "\\" + file;
	        }

	        FileInputStream input = new FileInputStream(dest);
	        try {
	            properties.load(input);
	           value = properties.getProperty(keyParam, "vide");
	       }catch(Exception e){
	           e.printStackTrace();
	       }
	        
	           return value;
	    }
	
	/**
	 * Sends an email and returns a boolean to inform if the operation was
	 * successful or not.
	 * 
	 * @author Chlo� Daudin Clavaud
	 * 
	 * @param mail       the email address of the recipient of this email.
	 * @param headerMail the header of this email.
	 * @param bodyMail   the text to insert in the body of this email.
	 * @param attachment the path to the attachment to join to this email.
	 * 
	 * @return a boolean indicating if the operation was successful or not.
	 * 
	 * @throws IOException If the configuration file cannot be read.
	 * 
	 */
	@SuppressWarnings("finally")
	public static boolean sendMail(String mail, String headerMail, String bodyMail, String attachment, String path)
			throws IOException {
		System.out.println(path);
		boolean success = false;
		int maxSize = 250;

		controlMail(mail, maxSize);

		// Create connection session
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", Tools.readFile("com.mail.host", path));
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.host", Tools.readFile("com.mail.smtphost", path));
		properties.put("mail.smtp.port", Tools.readFile("com.mail.port", path));
		properties.setProperty("mail.smtp.user", Tools.readFile("com.mail.user", path));
		properties.setProperty("mail.from", Tools.readFile("com.mail.from", path));
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		Session session = Session.getInstance(properties);

		// Prepare attachment insertion
		File file = new File(attachment);
		System.out.println(file.getAbsolutePath());
		FileDataSource datasource1 = new FileDataSource(file);
		DataHandler handler1 = new DataHandler(datasource1);
		MimeBodyPart attachedFile = new MimeBodyPart();
		try {
			attachedFile.setDataHandler(handler1);
			attachedFile.setFileName(datasource1.getName());
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Prepare message content
		MimeBodyPart content = new MimeBodyPart();
		try {
			content.setContent(bodyMail, "text/plain");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Put attachment and message content together
		MimeMultipart mimeMultipart = new MimeMultipart();
		try {
			mimeMultipart.addBodyPart(content);
			mimeMultipart.addBodyPart(attachedFile);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Create message
		MimeMessage message = new MimeMessage(session);
		try {
			message.setContent(mimeMultipart);
			message.setSubject(headerMail);
			message.addRecipients(Message.RecipientType.TO, mail);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Send message
		Transport transport = null;
		try {
			transport = session.getTransport("smtp");
			transport.connect(Tools.readFile("com.mail.from", path), Tools.readFile("com.mail.pwd", path));
			
			transport.sendMessage(message, new Address[] { new InternetAddress(mail) });
			success = true;
		} catch (MessagingException e) {
			e.printStackTrace();
			success = false;
		} finally {
			try {
				if (transport != null) {
					transport.close();
				}
			} catch (final MessagingException e) {
				e.printStackTrace();
			}
			return success;
		}
	}
	
	
	// Mail with no attachment
	public static boolean sendMail(String mail, String headerMail, String bodyMail, String path)
			throws IOException {
		System.out.println(path);
		boolean success = false;
		int maxSize = 250;

		controlMail(mail, maxSize);

		// Create connection session
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", Tools.readFile("com.mail.host", path));
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.host", Tools.readFile("com.mail.smtphost", path));
		properties.put("mail.smtp.port", Tools.readFile("com.mail.port", path));
		properties.setProperty("mail.smtp.user", Tools.readFile("com.mail.user", path));
		properties.setProperty("mail.from", Tools.readFile("com.mail.from", path));
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		Session session = Session.getInstance(properties);

		

		// Prepare message content
		MimeBodyPart content = new MimeBodyPart();
		try {
			content.setContent(bodyMail, "text/plain");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Put attachment and message content together
		MimeMultipart mimeMultipart = new MimeMultipart();
		try {
			mimeMultipart.addBodyPart(content);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Create message
		MimeMessage message = new MimeMessage(session);
		try {
			message.setContent(mimeMultipart);
			message.setSubject(headerMail);
			message.addRecipients(Message.RecipientType.TO, mail);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		// Send message
		Transport transport = null;
		try {
			transport = session.getTransport("smtp");
			transport.connect(Tools.readFile("com.mail.from", path), Tools.readFile("com.mail.pwd", path));
			
			transport.sendMessage(message, new Address[] { new InternetAddress(mail) });
			success = true;
		} catch (MessagingException e) {
			e.printStackTrace();
			success = false;
		} finally {
			try {
				if (transport != null) {
					transport.close();
				}
			} catch (final MessagingException e) {
				e.printStackTrace();
			}
			return success;
		}
	}
/**
 * Method static to control phone number and check if it respects the standards.
 * 
 * @author Hus Sebastien
 * 
 * @since 2019/04/23
 * 
 * @param phone : is the phone which is tested
 * 
 * @return : phone number at good format or "" if there are not 10 numbers.
 */
    public static String toStandardPhone(String phone)
    {
        String phoneChecked = "";
        String retour = "";

        //Extract all the parts of the String phone which are numbers.
        for (int i = 0; i < phone.length(); i++) {
            String result;

            if(i == phone.length()){
                result = phone.substring(i);
            }else {
                result = phone.substring(i,i+1);
            }

            String regex = "^[0-9]$";
            boolean isNumber = Pattern.matches(regex, result);

            if(isNumber){
                 phoneChecked = phoneChecked + result ;
            }
        }
        
        //Check if the phone to be returned contains 10 numbers.
        if(phoneChecked.length() == 10) {
            retour = phoneChecked ;
        } else{
            retour = "" ;
        }

        return retour ;
    
    }

    
    public static void sendSMS(String path) throws IOException {
		
		SMS sms  = new SMS("vous etes abonne au service","",SMS.getURL(),SMS.getUSER(),SMS.getPASS(),SMS.getMsgStart(),SMS.getMsgEnd(), path);
		

		
		sms.encodeMessage(SMS.getMsg(),SMS.getMsgEncode());
		
		String message = SMS.getMsgEncode();
		
		
		
		URL url = new URL( SMS.getURL() + SMS.getUSER() + SMS.getPASS() + SMS.getMsgStart() + message + SMS.getMsgEnd() );
		
		// open the connection for the url
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		
		// get the response code
		int code = connection.getResponseCode();
		
		// display the response code
		System.out.println(code);
		
	
}
    
}
