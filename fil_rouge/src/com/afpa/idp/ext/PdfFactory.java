package com.afpa.idp.ext;

import java.io.File;

/*
 * @Author : Camille, Fab & Fred
 * This method create a new pdf document
 * then fill it up with Client and Subscriptions informations  	 
 */

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import com.afpa.idp.classes.*;
import com.afpa.idp.dao.*;

public class PdfFactory {
	private static String nom, prenom, path, dest;
	private static int mySubsId;
	

	/**
	 * 
	 * @param client
	 * @param subscription
	 * @throws DocumentException
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 * Create new doc, Define save path, Open doc, Edit and close.
	 */
	public static void createPdf(Client client,Subscription subscription)
			throws DocumentException, IOException, FileNotFoundException, SQLException {
		
		
	
		
		float htPrice = subscription.getService().getPrice();
		double tva20 = (htPrice * 20) / 100;
		double ttcPrice = htPrice + tva20;
		

		BaseColor headerTable = new BaseColor(89, 93, 229);
		Font bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		Font regular = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);
		Font headerTableFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.WHITE);
		// Define save path
		//path = subscription.getService().getPath();
		String path="C:\\Users\\Dorian\\git\\fil_rouge_dorian\\fil_rouge\\conf";
		System.out.println(path);
		dest = path  + "\\Facture" + mySubsId + "" + client.getName() + ".pdf";
		
		System.out.println(dest);

		String company = Tools.readFile("com.company.name", path);
		String companyAdress = Tools.readFile("com.company.address", path);
		String companyZipCode = Tools.readFile("com.company.zipcode", path);
		String companyCity = Tools.readFile("com.company.city", path);
		String logo = path + "/img/logo.jpg";

		// New doc creation
		Document document = new Document();

		PdfWriter.getInstance(document, new FileOutputStream(dest));
		document.open();

		// Insert image
		Image image1 = Image.getInstance(logo);
		document.add(image1);

		// Main table with two locations that will contain a table
		PdfPTable mainTable = new PdfPTable(2);

		// Set spacing above element
		mainTable.setSpacingBefore(10f);

		// Set table width in %
		mainTable.setWidthPercentage(100);

		// Define first cell that will contain first table
		PdfPCell firstTableCell = new PdfPCell();
		firstTableCell.setBorder(PdfPCell.NO_BORDER);

		// Define table for first table cell
		PdfPTable firstTable = new PdfPTable(1);
		firstTable.setWidthPercentage(50);
		firstTable.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Define default settings for this table
		PdfPCell defaultClientCell1 = firstTable.getDefaultCell();
		defaultClientCell1.setBorder(PdfPCell.NO_BORDER);

		firstTable.addCell(new Paragraph(company, bold));
		firstTable.addCell(new Paragraph(companyAdress, regular));
		firstTable.addCell(new Paragraph(companyZipCode + " " + companyCity, regular));

		// Add table to cell, add cell to mainTable
		firstTableCell.addElement(firstTable);
		mainTable.addCell(firstTableCell);

		// Define second second cell that will contain the other table
		PdfPCell secondTableCell = new PdfPCell();
		secondTableCell.setBorder(PdfPCell.NO_BORDER);

		// Define table for the second table cell
		PdfPTable secondTable = new PdfPTable(1);
		secondTable.setWidthPercentage(65);
		secondTable.setHorizontalAlignment(Element.ALIGN_RIGHT);

		// Define default settings for this table
		PdfPCell defaultClientCell2 = secondTable.getDefaultCell();
		defaultClientCell2.setBorder(PdfPCell.NO_BORDER);

		secondTable.addCell(new Paragraph(client.getName() + " " + client.getFirstname(), bold));
		secondTable.addCell(new Paragraph(client.getAddress(), regular));
		secondTable.addCell(new Paragraph(client.getZipcode() + " " + client.getVille(), regular));

		// secondTable.addCell(cell);
		secondTableCell.addElement(secondTable);
		mainTable.addCell(secondTableCell);

		// paragraph.add(mainTable);
		document.add(mainTable);

		// New tab for bill info
		PdfPTable billInfos = new PdfPTable(1);

		// Set spacing above element
		billInfos.setSpacingBefore(10f);

		// Set table width in %
		billInfos.setWidthPercentage(100);

		// Set no border by default
		PdfPCell defaultBillInfosCell = billInfos.getDefaultCell();
		defaultBillInfosCell.setBorder(PdfPCell.NO_BORDER);

		// --- Insert Subscription.subscriptionId et subscriptionDate
		billInfos.addCell(new Paragraph("Facture n�" + " " + mySubsId, bold));
		billInfos.addCell(new Paragraph("Date : " + " " + subscription.getDate(), regular));

		document.add(billInfos);

		// Create table, first cell with large width
		PdfPTable table = new PdfPTable(new float[] { 10, 1 });
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
		// table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
		table.getDefaultCell().setBorderWidth(1);
		table.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);

		// Set spacing above element
		table.setSpacingBefore(10f);

		// Set table width in %
		table.setWidthPercentage(100);

		table.addCell(new Paragraph("Description", headerTableFont));
		table.addCell(new Paragraph("Prix", headerTableFont));

		table.setHeaderRows(1);
		PdfPCell[] cells = table.getRow(0).getCells();
		cells[0].setPaddingLeft(10);

		for (int j = 0; j < cells.length; j++) {
			cells[j].setBackgroundColor(headerTable);
			cells[j].setMinimumHeight(30);
		}

		table.addCell(new Paragraph("Windows 10", regular));
		table.addCell(new Paragraph(Double.toString(htPrice) + " �", regular));

		table.addCell(new Paragraph("Total HT", bold));
		table.addCell(new Paragraph(Double.toString(htPrice) + " �", bold));

		table.addCell(new Paragraph("TVA 20%", regular));
		table.addCell(new Paragraph(Double.toString(tva20) + " �", regular));

		table.addCell(new Paragraph("Total TTC", bold));
		table.addCell(new Paragraph(Double.toString(ttcPrice) + " �", bold));

		document.add(table);

		document.close();
	}
	/**
	 * @return the nom
	 */
	public static String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 */
	public static void setNom(String nom) {
		PdfFactory.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public static String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 */
	public static void setPrenom(String prenom) {
		PdfFactory.prenom = prenom;
	}
	/**
	 * @return the path
	 */
	public static String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public static void setPath(String path) {
		PdfFactory.path = path;
	}
	/**
	 * @return the dest
	 */
	public static String getDest() {
		return dest;
	}
	/**
	 * @param dest the dest to set
	 */
	public static void setDest(String dest) {
		PdfFactory.dest = dest;
	}
	/**
	 * @return the mySubsId
	 */
	public static int getMySubsId() {
		return mySubsId;
	}
	/**
	 * @param mySubsId the mySubsId to set
	 */
	public static void setMySubsId(int mySubsId) {
		PdfFactory.mySubsId = mySubsId;
	}
	
	
}