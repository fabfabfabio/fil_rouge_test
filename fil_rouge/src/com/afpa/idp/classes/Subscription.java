/** @author Blondiaux Dorian
	@author Leclercq Jimmy 
	@author Group 4
**/
package com.afpa.idp.classes;

import java.util.Date;

public class Subscription {

	private int subscriptionId;
	private Date date;
	private boolean status;
	private Service service;
	private User user;

	/**
	 * @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 * @param subscriptionId Id of the subscription
	 * @param date date of the subscription
	 * @param status Status of the subscription 
	 * @param service Service of this subscription
	 * @param user User of this subscription
	 */
	public Subscription(int subscriptionId, Date date, boolean status, Service service, User user) {
		this.subscriptionId = subscriptionId;
		this.date = date;
		this.status = status;
		this.service = service;
		this.user = user;
	}
	
	
	/**
	 * 
	 * @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 * @param date date of the subscription
	 * @param status Status of the subscription 
	 * @param service Service of this subscription
	 * @param user User of this subscription
	 */
	public Subscription(boolean status, Service service, User user) {
		this.date = date;
		this.status = status;
		this.service = service;
		this.user = user;
	}
	
/**
 *  @author Blondiaux Dorian
 * @author Leclercq Jimmy 
 * @author Group 4
 * @return To complete
 */
	public boolean generateBill() {

		return false;
	}
	
	/**
	 * Getter/Setters
	 */
	public int getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(int subscriptionId) {
		this.subscriptionId = subscriptionId;
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean getStatus() {
		return status;
	}
	
	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}	
	
	
}