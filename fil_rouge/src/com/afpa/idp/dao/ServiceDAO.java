/**
 * @author Blondiaux Dorian
 */

package com.afpa.idp.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.afpa.idp.classes.*;

public class ServiceDAO extends DAO<Service>{

	/**
	 * @author Blondiaux Dorian
	 */
	public ServiceDAO(Connection conn) {
		super(conn);
	}

	/**
	 * @author Blondiaux Dorian
	 */
	@Override
	public Service create(Service obj)   {
		// TODO Auto-generated method stub
		try {
			Statement st = null;
			st = this.connect.createStatement();  
			boolean test=false;
			if(obj.getVmType().contentEquals("Qemu")) {
				test=true;
			}
			String query = "insert into service (serviceId, serviceName, price, type) VALUES ("+ obj.getServiceId()+", '"+ obj.getServiceName() +"', "+ obj.getPrice() +", "+ test +")";
			st.executeUpdate(query);
		}catch(Exception e) {

		}
		DAO<Service> serviceDao = new ServiceDAO(this.connect);
		Service service = serviceDao.find(obj.getServiceId());
		return service;
	}

	/**
	 * @author Blondiaux Dorian
	 */
	@Override
	public Service delete(Service obj) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @author Blondiaux Dorian
	 */
	@Override
	public Service update(Service obj) {
		try {
			boolean test=false;
			if(obj.getVmType().contentEquals("Qemu")) {
				test=true;
			}
			this .connect	
			.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE, 
					ResultSet.CONCUR_UPDATABLE
					).executeUpdate(
							"UPDATE service SET serviceName = '" + obj.getServiceName()+ "', "+
									"price = " + obj.getPrice() + ", type=" + test +
									" WHERE serviceId = " + obj.getServiceId()
							);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		DAO<Service> serviceDao = new ServiceDAO(this.connect);
		Service service = serviceDao.find(obj.getServiceId());
		return service;
	}

	/**
	 * @author Blondiaux Dorian
	 */
	@Override
	public Service find(int id) {
		Service service = null;

		try {
			ResultSet result = this.connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM service WHERE serviceId = " + id);
			while (result.next()) {
				//System.out.println(result.getInt("serviceId")+" "+result.getString("serviceName")+ " " +result.getInt("price")+" " +result.getBoolean("type"));
				service=new Service(result.getInt("serviceId"),result.getString("serviceName"),result.getInt("price"),result.getBoolean("type"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// TODO Auto-generated method stub
		return service;
	}



	@Override
	public Service find(String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Service> find() {
		ArrayList<Service> services = new ArrayList<Service>();
		Service service = null;

		try {
			ResultSet result = this.connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM service");
			while (result.next()) {
				//System.out.println(result.getInt("serviceId")+" "+result.getString("serviceName")+ " " +result.getInt("price")+" " +result.getBoolean("type"));
				service=new Service(result.getInt("serviceId"),result.getString("serviceName"),result.getInt("price"),result.getBoolean("type"));
				services.add(service);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// TODO Auto-generated method stub
		return services;

	}
}