package com.afpa.idp.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.afpa.idp.classes.*;

public class ClientDAO extends DAO<Client> {

	public ClientDAO(Connection conn) {
		super(conn);
	}

	/**
	 * @group 4
	 * @author Blondiaux Dorian
	 * @author Leclercq Jimmy
	 */
	public Client create(Client client) {
		Client cli = null;

		try {
			DAO<User> userDao = new UserDAO(this.connect);
			User userX = userDao.create(client.getUser());
			System.out.println(userX.toString());
			client.setUser(userX);
			ResultSet result;

			
			System.out.println(client.toString());
			Statement st=null; 
			st = this.connect.createStatement();  
			
			java.sql.Date sql = new java.sql.Date(client.getBirthdate().getTime());
			 st.executeUpdate("INSERT INTO client(name, firstName, mail, phone, birthday, adress, zipCode, city, country, userId) "
							+ "VALUES ('"+client.getName()+"','"
							+client.getFirstname()+"','"
							+client.getMail()+"','"
							+client.getPhone()+"','"
							+sql+"','"
							+client.getAddress()+"','"
							+client.getZipcode()+"','"
							+client.getVille()+"','"
							+client.getPays()+"', " + 
							+userX.getUserId()
							+");");
			
			String query = "SELECT * FROM client ORDER BY clientId DESC LIMIT 0, 1";
			result = this.connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY).executeQuery(query);

			result.next();
			if(result.first()) {

				cli = new Client(
						result.getInt("clientId"),
						result.getString("name"),
						result.getString("firstName"),
						result.getString("mail"),
						result.getString("phone"),
						result.getDate("birthday"),
						result.getString("adress"),
						result.getInt("zipCode"),
						result.getString("city"),
						result.getString("country"),
						userX
						);
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		return cli;
	}

	public Client delete(Client client) {
		return null;
	}

	/**
	 * @group 4
	 * @author Blondiaux Dorian
	 * @author Leclercq Jimmy
	 */
	public Client update(Client client) {
		try {
			java.sql.Date sql = new java.sql.Date(client.getBirthdate().getTime());
			String querry ="UPDATE client SET clientId=" + client.getId()
			+ ", name = '"+client.getName()+"', "
			+"firstName = '"+client.getFirstname()+"', "
			+"mail = '"+client.getMail()+"', "
			+"phone = '"+client.getPhone()+"', "
			+"birthday = '"+sql+"', "
			+"adress = '"+client.getAddress()+"', "
			+"zipCode = "+client.getZipcode()+", "
			+"city = '"+client.getVille()+"', "
			+"country = '"+client.getPays()
			+"' WHERE clientId = "+client.getId()+";"
			;
			this.connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY).executeUpdate(querry);
			DAO<User> userDao = new UserDAO(this.connect);
			userDao.update(client.getUser());
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return client;
	}

	/**
	 * @group 4
	 * @author Blondiaux Dorian
	 * @author Leclercq Jimmy
	 */
	//Find client with user id
	public Client find(int id) {
		Client client =null;
		try {
			ResultSet result = this.connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM client WHERE userId = " + id);
			result.next();
			if(result.first()) {
				DAO<User> userDao = new UserDAO(this.connect);
				User user = userDao.find(result.getInt("userId"));
				client = new Client(
						result.getInt("clientId"),
						result.getString("name"),
						result.getString("firstName"),
						result.getString("mail"),
						result.getString("phone"),
						result.getDate("birthday"),
						result.getString("adress"),
						result.getInt("zipCode"),
						result.getString("city"),
						result.getString("country"),
						user
						);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return client;
	}

	@Override
	public Client find(String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Client> find() {
		// TODO Auto-generated method stub
		return null;
	}
}