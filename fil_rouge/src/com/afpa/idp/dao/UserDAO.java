/**
* @author Blondiaux Dorian
*/
package com.afpa.idp.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import com.afpa.idp.classes.Service;
import com.afpa.idp.classes.Subscription;
import com.afpa.idp.classes.User;

public class UserDAO extends DAO<User>{
	
	/**
	* @author Blondiaux Dorian
	*/
	  public UserDAO(Connection conn) {
		    super(conn);
		  }
	  
	  /**
	  * @author Blondiaux Dorian
	  */
		  public User create(User obj) {
			  User user =null;
			  try {
					Statement st = null;
					String query = "insert into user (userName, pwd) VALUES ('"+ obj.getUserName() +"', '"+ obj.getPassword() +"')";
					
					st = this.connect.createStatement();  
					
					st.executeUpdate(query);
					
					
					 query = "SELECT * FROM user ORDER BY userId DESC LIMIT 0, 1";
					 ResultSet result = this.connect.createStatement(
			                    ResultSet.TYPE_SCROLL_INSENSITIVE,
			                    ResultSet.CONCUR_READ_ONLY).executeQuery(query);
				
					 while (result.next()) {
						  user = new User(result.getInt("userId"), result.getString("userName"),result.getString("pwd"));
						  System.out.println(user.getUserId() + " " + user.getUserName() + " " + user.getPassword());
					 }
					}catch(Exception e) {
						
					}
			  		
			  	 
			  		//DAO<User> userDao = new UserDAO(this.connect);
			  		//System.out.println(obj.getUserName() +" " +obj.getPassword());
			  		//User user = userDao.find(obj.getUserName(),obj.getPassword());
			  
			  		if(obj.getSubscriptions()!=null && obj.getSubscriptions().size()>0 ) {
			  			
				  		DAO<Subscription> subscriptionDao = new SubscriptionDAO(this.connect);
				  		for(Subscription sub: obj.getSubscriptions()) {
				  			user.addSubscription(subscriptionDao.create(sub));
				  		}
			  		}
					
			  
		    return user;
		  }

		  public User delete(User obj) {
		    return null;
		  }
		   
		  /**
		  * @author Blondiaux Dorian
		  */
		  public User update(User obj) {
			  try {
	                this .connect	
	                     .createStatement(
	                    	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                        ResultSet.CONCUR_UPDATABLE
	                     ).executeUpdate(
	                    	"UPDATE user SET userName = '" + obj.getUserName()+ "', "+
	                    			"pwd = '" + obj.getPassword() + "' WHERE userId= " + obj.getUserId()
	                     );
	                
		    } catch (SQLException e) {
		            e.printStackTrace();
		    }
			  DAO<User> userDao = new UserDAO(this.connect);
			  User user = userDao.find(obj.getUserName(),obj.getPassword());
				return user;
		  }

		  
		  /**
		  * @author Blondiaux Dorian
		  */
		public User find(String userName, String password) {
			  User user = new User();      
			  ArrayList<Subscription> subscriptions = new ArrayList<Subscription>();
			    try {
			    	System.out.println("SELECT * FROM user WHERE userName = '" + userName + "' AND pwd = '" + password+ "'");
			      ResultSet result = this.connect.createStatement(
			        ResultSet.TYPE_SCROLL_INSENSITIVE,
			        ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM user WHERE userName = '" + userName + "' AND pwd = '" + password+ "'");
			      
			      
			      
			      if(result.first()) {
			    	  user = new User(result.getInt("userId"),result.getString("userName"),result.getString("pwd"));         
			      }
			        
			      
					  result = this.connect.createStatement(
						        ResultSet.TYPE_SCROLL_INSENSITIVE,
						        ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM subscription, user  WHERE user.userId=subscription.userId AND user.userId='" + user.getUserId() + "'");
					 while (result.next()) {
						 Date date = result.getDate("dateSubscription");
						 
						 //APPEL ServiceDao
						 DAO<Service> serviceDao = new ServiceDAO(connect);
						 Service service = serviceDao.find(result.getInt("serviceId"));
						 	Subscription subs = new Subscription(result.getInt("subscriptionId"),  date, result.getBoolean("status"), service,  user);
						 	subscriptions.add(subs);
						}
					 
			    } catch (SQLException e) {
			      e.printStackTrace();
			    }
			    user.setSubscriptions(subscriptions);
			    return user;
		}

	

		/**
		* @author Blondiaux Dorian
		*/
		@Override
		public User find(int id) {
			  User user = new User();      
			  ArrayList<Subscription> subscriptions = new ArrayList<Subscription>();
			    try {
			      ResultSet result = this.connect.createStatement(
			        ResultSet.TYPE_SCROLL_INSENSITIVE,
			        ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM user WHERE userId = " + id);
			      
			      
			      
			      if(result.first()) {
			    	  user = new User(result.getInt("userId"),result.getString("userName"),result.getString("pwd"));         
			      }
			        
			      
					  result = this.connect.createStatement(
						        ResultSet.TYPE_SCROLL_INSENSITIVE,
						        ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM subscription, user  WHERE user.userId=subscription.userId AND user.userId='" + user.getUserId() + "'");
					 while (result.next()) {
						 Date date = result.getDate("dateSubscription");
						 
						 //APPEL ServiceDao
						 DAO<Service> serviceDao = new ServiceDAO(connect);
						 Service service = serviceDao.find(result.getInt("serviceId"));
						 	Subscription subs = new Subscription(result.getInt("subscriptionId"),  date, result.getBoolean("status"), service,  user);
						 	subscriptions.add(subs);
						}
					 
			    } catch (SQLException e) {
			      e.printStackTrace();
			    }
			    user.setSubscriptions(subscriptions);
			    return user;
		}

		@Override
		public ArrayList<User> find() {
			ArrayList<User> users = new ArrayList<User>();
			User user = null;

			try {
				ResultSet result = this.connect.createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM user");
				while (result.next()) {
					//System.out.println(result.getInt("serviceId")+" "+result.getString("serviceName")+ " " +result.getInt("price")+" " +result.getBoolean("type"));
					user=new User(result.getInt("userId"),result.getString("userName"),result.getString("pwd"));
					users.add(user);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

			// TODO Auto-generated method stub
			return users;
		}

		
}