/**
	 * @group 4
	 * @author Blondiaux Dorian
	 * @author Leclercq Jimmy
*/

package com.afpa.idp.dao;

import java.sql.Connection;
import java.util.ArrayList;

import com.afpa.idp.classes.User;

public abstract class DAO<T> {
	  protected Connection connect = null;
	   
	  public DAO(Connection conn){
	    this.connect = conn;
	  }
	   
	  /**
	  * M�thode de cr�ation
	  * @param obj
	  * @return boolean 
	  */
	  public abstract T create(T obj);

	  /**
	  * M�thode pour effacer
	  * @param obj
	  * @return boolean 
	  */
	  public abstract T delete(T obj);

	  /**
	  * M�thode de mise � jour
	  * @param obj
	  * @return boolean
	  */
	  public abstract T update(T obj);
	  
	  /**
	  * M�thode de recherche des informations
	  * @param id
	  * @return T
	   */
	  public abstract T find(int id);
	  
	  //FindAll
	  public abstract ArrayList<T> find();

	public abstract T find(String string, String string2);

	

}
