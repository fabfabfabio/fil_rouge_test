/**
* @author Blondiaux Dorian
*/
package com.afpa.idp.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.afpa.idp.classes.*;

public class SubscriptionDAO extends DAO<Subscription> {

	/**
	* @author Blondiaux Dorian
	*/
	public SubscriptionDAO(Connection conn) {
		super(conn);
		// TODO Auto-generated constructor stub
	}

	/**
	* @author Blondiaux Dorian
	*/
	@Override
	public Subscription create(Subscription obj) {
		Subscription subs=null;
		try {
			Statement st = null;
	        st = this.connect.createStatement();  
			String query = "insert into subscription ( userId, serviceId, status, dateSubscription) VALUES ( "+ obj.getUser().getUserId() +", "+ obj.getService().getServiceId() + ", " + obj.getStatus() +", NOW())";
			
			st.executeUpdate(query);
			 query = "SELECT * FROM subscription ORDER BY subscriptionId DESC LIMIT 0, 1";
			 ResultSet result = this.connect.createStatement(
	                    ResultSet.TYPE_SCROLL_INSENSITIVE,
	                    ResultSet.CONCUR_READ_ONLY).executeQuery(query);
			 while (result.next()) {
	            	DAO<User> userDao = new UserDAO(this.connect);
	  			    User user = userDao.find(result.getInt("userId"));
	  			    DAO<Service> serviceDao = new ServiceDAO(this.connect);
	  			    Service service = serviceDao.find(result.getInt("serviceId"));
			        subs=new Subscription(result.getInt("subscriptionId"),result.getDate("dateSubscription"),result.getBoolean("status"),service, user);
			    }
			}catch(Exception e) {
				
			}
			return subs;
	}

	@Override
	public Subscription delete(Subscription obj) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	* @author Blondiaux Dorian
	*/
	@Override
	public Subscription update(Subscription obj) {
		try {
			String req="UPDATE subscription SET subscriptionId = " + obj.getSubscriptionId()+ ", "+
        			"userId = " + obj.getUser().getUserId() +", " +
        			"serviceId = " + obj.getService().getServiceId() +
        			", status ="+ obj.getStatus() +
        			", dateSubscription=NOW()" +
        			"WHERE subscriptionId= " + obj.getSubscriptionId();
            this .connect	
                 .createStatement(
                	ResultSet.TYPE_SCROLL_INSENSITIVE, 
                    ResultSet.CONCUR_UPDATABLE
                 ).executeUpdate(req
                	
                 );
            
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
		  DAO<Subscription> subscriptionDao = new SubscriptionDAO(this.connect);
		  Subscription subscription = subscriptionDao.find(obj.getSubscriptionId());
			return subscription;
	}

	/**
	* @author Blondiaux Dorian
	*/
	@Override
	public Subscription find(int id) {
		Subscription subs = null;
		
	    try {
            ResultSet result = this.connect.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM subscription WHERE subscriptionId = " + id);
            
            while (result.next()) {
            	DAO<User> userDao = new UserDAO(this.connect);
  			    User user = userDao.find(result.getInt("userId"));
  			    DAO<Service> serviceDao = new ServiceDAO(this.connect);
  			    Service service = serviceDao.find(result.getInt("serviceId"));
		    	subs=new Subscription(result.getInt("subscriptionId"),result.getDate("dateSubscription"),result.getBoolean("status"),service, user);
		    }
        } catch (SQLException e) {
            e.printStackTrace();
        }
	    
		return subs;
	}

	@Override
	public Subscription find(String string, String string2) {

		return null;
	}

	@Override
	public ArrayList<Subscription> find() {
		// TODO Auto-generated method stub
		return null;
	}

}
