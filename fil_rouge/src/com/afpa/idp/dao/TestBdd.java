/**
 *  @author Blondiaux Dorian
		@author Leclercq Jimmy 
		@author Group 4
 */


package com.afpa.idp.dao;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import com.afpa.idp.classes.Client;
import com.afpa.idp.classes.Service;
import com.afpa.idp.classes.Subscription;
import com.afpa.idp.classes.User;

public class TestBdd {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, FileNotFoundException, ParseException {
		// TODO Auto-generated method stub
		
		/*String port = "3006";
	    String base ="idp_services";
	    String url = "jdbc:mysql://localhost/"+ base;
	    Class.forName( "com.mysql.jdbc.Driver" );
		Connection connexion = DriverManager.getConnection( url, "root", "" );
		
		DAO<User> societeDao = new UserDAO(connexion);
		
		System.out.println(societeDao.find("richard","richard1234").getUserId());*/
		
		//ITEM CONNEXION
		String port = "3006";
	    String base ="idp_services";
	    String url = "jdbc:mysql://localhost/"+ base;
	    Class.forName( "com.mysql.jdbc.Driver" );
		 Connection connexion = DriverManager.getConnection( url, "root", "" );
		User user = new User(1, "test", "test");
		
		/*Date date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Service service =  new Service(106, "Windows 10",98, true);
		Bdd.findService(106, connexion);
		Subscription subs = new Subscription(date, true, service, user);
		//Bdd.insertSubscription(subs, connexion);
		ArrayList<Subscription> subscriptions = Bdd.getSubscriptions(user, connexion);
		for(Subscription sub: subscriptions) {
			System.out.println(sub.getService().getServiceName() +" " + sub.getUser().getUserName());
		}*/
		
		//Test DAO SERVICE 
		/*
		 DAO<Service> serviceDao = new ServiceDAO(connexion);
		 //Affichage
		 Service service = serviceDao.find(105);
		 System.out.println(service.getServiceName() + " " + service.getPrice());
		 //Update
		 service.setPrice(56);
		 serviceDao.update(service);
		 service = serviceDao.find(105);
		 System.out.println(service.getServiceName() + " " + service.getPrice());
		 
		 //TEST DAO USER
		 DAO<User> userDao = new UserDAO(connexion);
		 //Affichage
		 User user2 = userDao.find("test","test");
		 System.out.println(user2.getUserId());
		 for(Subscription sub: user2.getSubscriptions()) {
				System.out.println(sub.getService().getServiceName());
			}
		 //Insertion (MANQUE INSERTION SUBSCRIPTIONS)
		 User user3 = new User("toto","tata");
		 userDao.create(user3);
		 System.out.println(userDao.find("toto", "tata").getUserId());
		 //TEST INSERTION AVEC SUBSCRIPTIONS 
		 User userT=userDao.find("test", "test");
		 userT.setUserName("testClone5");
		 //System.out.println(userT.getSubscriptions().get(0).getService().getServiceName());
		 userT=userDao.create(userT);
		 //Update
		 User user4 = userDao.find("toto", "tata");
		 user4.setPassword("ataoy");
		 user4 = userDao.update(user4);
		 System.out.println(user4.getPassword());
		 */
		 //TEST DAO SUBSCRIPTION
		 //Find
		 DAO<Subscription> subscriptionDAO = new SubscriptionDAO(connexion);
		 Subscription subs = subscriptionDAO.find(4);
		 System.out.println(subs.getUser().getUserName());
		 /*
		 //Insert
		 Date date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		 Subscription subTest = new Subscription(true, serviceDao.find(105), userDao.find(4));
		 System.out.println(subscriptionDAO.create(subTest).getSubscriptionId());*/
		 //Update
		 /*
		 Subscription subTest2 = subscriptionDAO.find(7);
		 subTest2.setStatus(false);
		 subTest2=subscriptionDAO.update(subTest2);
		 System.out.println(subs.getStatus());
		 */
		 //TEST CLIENT
		 
		 //INSERTION
		 DAO<Client> clientDAO = new ClientDAO(connexion);
		 User userY = new User("pika","chu");
		 /*
		 SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
		 
		 java.util.Date date1 = dt1.parse("22/09/1996");
		 java.sql.Date sql = new java.sql.Date(date1.getTime());
		 
		 Client client = new Client("azeazeaezaeza","uyti","iuih","hiu",sql,"hggh",59000,"akzea","lkoj",userY);
		 client = clientDAO.create(client);
		 System.out.println(client.getId());
		 */
		 //AFFICHAGE
		 Client client1 = clientDAO.find(1);
		 System.out.println(client1.getUser().getUserName());
		 //UPDATE
		 client1.getUser().setPassword("chuuuuuuu");
		 client1=clientDAO.update(client1);
		 System.out.println(client1.getUser().getPassword());
	}

}
