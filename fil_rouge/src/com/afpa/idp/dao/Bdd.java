

package com.afpa.idp.dao;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

import com.afpa.idp.classes.*;
public class Bdd {
	
	
	
	//SELECT
	/**
	 * @author Blondiaux Dorian
	 * @param user User of the subscriptions we want
	 * @param connexion Connexion chain
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<Subscription> getSubscriptions(User user, Connection connexion) throws ClassNotFoundException, SQLException{
		 ArrayList<Subscription> subscriptions = new ArrayList<Subscription>();
		 ResultSet result = connexion.createStatement(
			        ResultSet.TYPE_SCROLL_INSENSITIVE,
			        ResultSet.CONCUR_READ_ONLY).executeQuery("SELECT * FROM subscription, user  WHERE user.userId=subscription.userId AND user.userId='" + user.getUserId() + "'");
		 while (result.next()) {
			 Date date = result.getDate("dateSubscription");
			 Service service = findService(result.getInt("serviceId"), connexion);
			 	Subscription subs = new Subscription(result.getInt("subscriptionId"),  date, result.getBoolean("status"), service,  user);
			 	subscriptions.add(subs);
			    //System.out.println(result.getString(1) +" "+ result.getString(2) +" " + result.getString(3));
			}
		return subscriptions;
		
		
	}
	
	/**
	 * @author Chlo� Daudin Clavaud
	 * @author Blondiaux Dorian
	 * @param serviceId is the id of the service
	 * @return an object service with all the parameters corresponding to this serviceId in the database
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Service findService(int serviceId, Connection conn) throws ClassNotFoundException, SQLException {
		
        Service service = null;
        // String userName, String psw

        Statement st = null;

        st = conn.createStatement();  
       // System.out.println(serviceId);
		String query = "SELECT * FROM service WHERE serviceId = " + serviceId;
		//System.out.println(query);
		
		ResultSet rs2 = st.executeQuery(query);
		while (rs2.next()) {
		String serviceName  = rs2.getString("serviceName");
		int price  = rs2.getInt("price");
		boolean type = rs2.getBoolean("type");
		//System.out.println("Bool:"+ type);
		
		 service = new Service(serviceId, serviceName, price, type);
		}
		return service;
	}
	

	/**
	 * @author Chlo� Daudin Clavaud
	 * @author Blondiaux Dorian
	 * @param serviceId is the id of the service
	 * @return an object service with all the parameters corresponding to this serviceId in the database
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<Service> findServices(Connection conn) throws ClassNotFoundException, SQLException {
		
        Service service = null;
        ArrayList<Service> services = new ArrayList<Service>();
        // String userName, String psw

        Statement st = null;

        st = conn.createStatement();  
		String query = "SELECT * FROM service";
		System.out.println(query);
		
		ResultSet rs2 = st.executeQuery(query);
		while (rs2.next()) {
		String serviceName  = rs2.getString("serviceName");
		int price  = rs2.getInt("price");
		boolean type = rs2.getBoolean("type");
		System.out.println("Bool:"+ type);
		
		 service = new Service(rs2.getInt("serviceId"), serviceName, price, type);
		 services.add(service);
		}
		return services;
	}
	
	/**
	 * @author Chlo� Daudin Clavaud
	 * 
	 * @param userName is the login of the user
	 * @param pwd is the password of the user
	 * 
	 * @return an object user with all the parameters corresponding to this userName and pwd in the database
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static User findUser(String userName, String pwd, Connection conn) throws ClassNotFoundException, SQLException {
	

        Statement st = null;
        User user=null;
       
        st = conn.createStatement();  
		String query = "SELECT * FROM user WHERE userName = '" + userName + "' AND pwd = '" + pwd +"'";
		ResultSet rs = st.executeQuery(query);
		while (rs.next()) {
			int userId  = rs.getInt("userId");
			
			 user = new User(userId, userName, pwd);
		}
		return user;
	}
	
	/**
	 * @author Blondiaux Dorian
	 * 
	 * @param userName is the login of the user
	 * @param pwd is the password of the user
	 * 
	 * @return an object user with all the parameters corresponding to this userName and pwd in the database
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static User findUser(int userId, Connection conn) throws ClassNotFoundException, SQLException {
	
		User user=null;
        Statement st = null;

       
        st = conn.createStatement();  
		String query = "SELECT * FROM user WHERE userId=" + userId ;
		ResultSet rs = st.executeQuery(query);
		
		while (rs.next()) {
		String userName = rs.getString("userName");
		String pwd = rs.getString("pwd");
		
	    user = new User(userId, userName, pwd);
		}
		return user;
	}
	
	//INSERT
	/**
	 * @author Fabrice SALAH
	 * @param subscription subscription to write in the database
	 * @param conn Connexion chain
	 * @return
	 * @throws FileNotFoundException
	 * @throws SQLException
	 */
	public static boolean insertSubscription(Subscription subscription, Connection conn) throws FileNotFoundException, SQLException {
		
		int userId = subscription.getUser().getUserId();
		int serviceId = subscription.getService().getServiceId();
		int status = 0;
		Statement st = null;
        st = conn.createStatement();  
		
		String query = "insert into subscription (userId, serviceId, status) VALUES ("+ userId +", "+ serviceId +", "+ status +")";
		st.executeUpdate(query);
		
		return true;
	} 
}
