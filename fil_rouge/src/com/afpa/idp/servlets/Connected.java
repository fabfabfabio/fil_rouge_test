package com.afpa.idp.servlets;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;

import com.afpa.idp.classes.Client;
import com.afpa.idp.classes.Service;
import com.afpa.idp.classes.Subscription;
import com.afpa.idp.classes.User;
import com.afpa.idp.dao.ClientDAO;
import com.afpa.idp.dao.Connexion;
import com.afpa.idp.dao.DAO;
import com.afpa.idp.dao.ServiceDAO;
import com.afpa.idp.dao.SubscriptionDAO;
import com.afpa.idp.dao.UserDAO;
import com.afpa.idp.ext.Password;
import com.afpa.idp.ext.PdfFactory;
import com.afpa.idp.ext.Tools;
import com.itextpdf.text.DocumentException;

/**
 * @author Blondiaux Dorian
 */
@WebServlet("/connected.do")
public class Connected extends HttpServlet {

	private static final long serialVersionUID = 1845342079984939476L;

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
		
		HttpSession session = request.getSession();
		
		//Check if user connected
		if(session.getAttribute("user")!=null){
	    	Connection connexion =null;
	    	User user = (User) session.getAttribute("user");
		    try {
				Connexion co = new Connexion();
				connexion=co.getConnection();
				request.setAttribute("services",getServices(connexion));
	
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			//Case launch service
			String servLaunch = request.getParameter( "servlaunch" );
			if(servLaunch!=null) {
				DAO<Service> serviceDao = new ServiceDAO(connexion);
				Service service = serviceDao.find(Integer.valueOf(servLaunch));
				try {
					Tools.writeLogs(service.getServiceId(),"LAUNCH SERVICE: ", service.toString());
					service.launchService();
				} catch (JSONException | InterruptedException | URISyntaxException e) {
					e.printStackTrace();
				}
	
			}
	
			//Case stop service
			String servStop = request.getParameter( "servstop" );
			if(servStop!=null) {
				DAO<Service> serviceDao = new ServiceDAO(connexion);
				Service service = serviceDao.find(Integer.valueOf(servStop));
				Tools.writeLogs(service.getServiceId(),"STOP SERVICE: ", service.toString());
				try {
					service.stopService();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
	
			
			
	//Case subscribe
			String subscribe = request.getParameter( "subscribeservice" );
			if(subscribe!=null) {
			    DAO<Service> serviceDao = new ServiceDAO(connexion);
				Service service = serviceDao.find(Integer.valueOf(subscribe));
				
				// Check if he is alrdy subscripted
				if(user.isSubscribed(service)==false) {
					Subscription subscription = new Subscription(true,service,user);
					DAO<Subscription> subscriptionDao = new SubscriptionDAO(connexion);
					subscription = subscriptionDao.create(subscription);
					
					user.addSubscription(subscription);
					
					DAO<Client> clientDao = new ClientDAO(connexion);
					Client cli = clientDao.find(user.getUserId());
					try {
						if(cli!=null) {
							//Send mail + bill
							Tools.writeLogs(user.getUserId(),"SUBSCRIPTION: ", cli.toString());
							PdfFactory.createPdf(cli,subscription);
							Tools.sendMail(cli.getMail(),"Votre facture","Bonjour voici votre facture",PdfFactory.getDest(),"C:\\Users\\Dorian\\git\\fil_rouge_dorian\\fil_rouge\\conf");
							Tools.sendSMS("C:\\Users\\Dorian\\git\\fil_rouge_dorian\\fil_rouge\\conf");
						}
						
					} catch (DocumentException | SQLException e) {
						e.printStackTrace();
					}
					
					session.setAttribute("user",user);
					this.getServletContext().getRequestDispatcher("/WEB-INF/connected.jsp").forward(request, response);
				}
		}
			//Case refresh services
			String servrefresh = request.getParameter( "servrefresh" );
			if(servrefresh!=null) {
				refresh(connexion, request, response);
			}
			//Other case
			if(servrefresh==null && subscribe==null && servStop==null && servLaunch==null) {
				refresh(connexion, request, response);
			}
	}else{
		this.getServletContext().getRequestDispatcher("/WEB-INF/notconnected.jsp").forward(request, response);
	}
}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		     
		String login = request.getParameter("username");
	    String pass = request.getParameter("pass");
	   
		//ICI
		 
	    String request1="SELECT userName, pwd from user where userName=?";
	    
	    try {
	    	Connexion co = new Connexion();
			Connection connexion=co.getConnection();
			request.setAttribute("services",getServices(connexion));
			PreparedStatement prepare = connexion.prepareStatement(request1);
			prepare.setString(1, login);
			ResultSet resultat = prepare.executeQuery(  );
			
			//If Logs match
			if(resultat.first() && Password.checkPassword(pass,resultat.getString("pwd"))) {
				System.out.println("co reussie");
				request.setAttribute("requete","co reussie");
				DAO<User> userDao = new UserDAO(connexion);
				User user = userDao.find(login,resultat.getString("pwd"));
				System.out.println(request.getServletContext().getRealPath("/")+"traces.log");
				 Tools.writeLogs(user.getUserId(),"CONNECTION: ", user.toString());
				request.setAttribute("user",user);
				request.setAttribute("services",getServices(connexion));
				
		        HttpSession session = request.getSession();

		        session.setAttribute("user", user);
		       
		        
				this.getServletContext().getRequestDispatcher("/WEB-INF/connected.jsp").forward(request, response);
			}else {
				this.getServletContext().getRequestDispatcher("/WEB-INF/test.jsp").forward(request, response);
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}catch(Exception e){
			this.getServletContext().getRequestDispatcher("/WEB-INF/test.jsp").forward(request, response);
		}

	}

	public ArrayList<Service> getServices(Connection co){
		DAO<Service> serviceDao = new ServiceDAO(co);
		ArrayList<Service> allServices= serviceDao.find();
		return allServices;
	}

	public void refresh(Connection connexion, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute( "user" );
		DAO<User> userDao = new UserDAO(connexion);
		user = userDao.find(user.getUserId());
		session.setAttribute("user", user);
		if(user!=null) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connected.jsp").forward(request, response);

		}
	}

}